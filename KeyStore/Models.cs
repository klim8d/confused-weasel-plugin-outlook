﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfusedWeasel.Outlook.KeyStore.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserUuid { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public User()
        {

        }

        public User(int _id, string _userUuid, string _userName, string _password)
        {
            Id = _id;
            UserUuid = _userUuid;
            UserName = _userName;
            Password = _password;
        }
    }

    public class UserKey
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }

        public UserKey()
        {

        }

        public UserKey(int _id, int _userId, string _email, string _privateKey, string _publicKey, DateTime _createdDate, bool _isActive)
        {
            Id = _id;
            UserId = _userId;
            Email = _email;
            PrivateKey = _privateKey;
            PublicKey = _publicKey;
            CreatedDate = _createdDate;
            IsActive = _isActive;
        }
    }

    public class PublicKey
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Key { get; set; }
        public DateTime CreatedDate { get; set; }

        public PublicKey()
        {

        }

        public PublicKey(int _id, int _userId, string _email, string _key, DateTime _createdDate)
        {
            Id = _id;
            UserId = _userId;
            Email = _email;
            Key = _key;
            CreatedDate = _createdDate;
        }
    }
}
