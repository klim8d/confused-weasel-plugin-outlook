﻿using System;
using System.IO;
using System.Data.SQLite;
using System.Collections.Generic;

namespace ConfusedWeasel.Outlook.KeyStore
{
    public class Resources : IDisposable
    {
        private string storageFileName;
        private SQLiteConnection db;
        private static Resources _instance;

        public static Resources GetInstance { get { return _instance ?? (_instance = new Resources()); } set { GetInstance = value; } }

        private Resources()
        {
            storageFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "keystore.dat");
            if (File.Exists(storageFileName) == true)
            {
                OpenConnection();
            }
            else
            {
                SQLiteConnection.CreateFile(storageFileName);

                OpenConnection();
                CreateTableStructure();
            }
        }

        public SQLiteTransaction BeginTransaction()
        {
            return db.BeginTransaction();
        }

        private int ExecuteWrite(string _query)
        {
            SQLiteCommand command = new SQLiteCommand(_query, db);
            return command.ExecuteNonQuery();
        }

        private SQLiteDataReader ExecuteRead(string _query)
        {
            SQLiteCommand command = new SQLiteCommand(_query, db);
            SQLiteDataReader reader = command.ExecuteReader();

            return reader;
        }

        private void OpenConnection()
        {
            db = new SQLiteConnection("Data Source=" + storageFileName + ";Version=3;");
            db.Open();
        }

        private void CreateTableStructure()
        {
            string query_user = "CREATE TABLE Users (Id INTEGER PRIMARY KEY AUTOINCREMENT, UserUuid TEXT NOT NULL, Username TEXT NOT NULL);";
            string query_ukey = "CREATE TABLE UserKeys(Id INTEGER PRIMARY KEY AUTOINCREMENT, UserId INT NOT NULL, Email TEXT NOT NULL, " +
                                "PrivateKey TEXT NOT NULL, PublicKey TEXT NOT NULL, CreatedDate NUMERIC NOT NULL, IsActive INTEGER, " +
                                "FOREIGN KEY(UserId) REFERENCES Users(Id));";
            string query_pkey = "CREATE TABLE PublicKeys(Id INTEGER PRIMARY KEY AUTOINCREMENT, UserId INT NOT NULL, Email TEXT NOT NULL, " +
                                "PublicKey TEXT NOT NULL, CreatedDate NUMERIC NOT NULL, " +
                                "FOREIGN KEY(UserId) REFERENCES Users(Id));";

            string query_final = query_user + query_ukey + query_pkey;

            ExecuteWrite(query_final);
        }

        public Models.User CreateUser(Models.User _user)
        {
            string query = string.Format("INSERT INTO Users (UserUuid, UserName) VALUES ('{0}', '{1}');",
                                          _user.UserUuid, _user.UserName);

            if (ExecuteWrite(query) == 1)
                return (_user = GetUserByUuid(_user.UserUuid));

            return null;
        }

        public Models.User GetUserById(int _id)
        {
            string query = string.Format("SELECT Id, UserUuid, UserName FROM Users WHERE Id = {0} LIMIT 1;", _id);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    Models.User user = new Models.User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), null);

                    return user;
                }
            }

            return null;
        }

        public Models.User GetUserByUuid(string _uuid)
        {
            string query = string.Format("SELECT Id, UserUuid, UserName FROM Users WHERE UserUuid = '{0}' LIMIT 1;", _uuid);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    Models.User user = new Models.User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), null);

                    return user;
                }
            }

            return null;
        }

        public Models.User GetUserByUserName(string _userName)
        {
            string query = string.Format("SELECT Id, UserUuid, UserName FROM Users WHERE UserName = '{0}' LIMIT 1;", _userName);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    Models.User user = new Models.User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), "");

                    return user;
                }
            }

            return null;
        }

        public bool DeleteUser(Models.User _user)
        {
            string query = string.Format("DELETE FROM Users WHERE Id = {0};", _user.Id);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public bool CreateUserKeySet(Models.UserKey _userKey)
        {
            string query = string.Format("INSERT INTO UserKeys (UserId, Email, PrivateKey, PublicKey, CreatedDate, IsActive) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}');",
                                         _userKey.UserId, _userKey.Email, _userKey.PrivateKey, _userKey.PublicKey, _userKey.CreatedDate, _userKey.IsActive);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public List<Models.UserKey> GetUserKeySetsByUser(Models.User _user)
        {
            List<Models.UserKey> set = new List<Models.UserKey>();

            string query = string.Format("SELECT Id, UserId, Email, PrivateKey, PublicKey, CreatedDate, IsActive FROM UserKeys WHERE UserId = {0};", _user.Id);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    var datetime = DateTime.ParseExact(reader.GetString(5), "dd/MM/yyyy HH:mm:ss", null);
                    var isActive = (reader.GetString(6) == "True") ? true : false;

                    Models.UserKey model = new Models.UserKey(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3),
                                                              reader.GetString(4), datetime, isActive);

                    set.Add(model);
                }

                return set;
            }

            return null;
        }

        public Models.UserKey GetUserKeySetByEmail(string _email)
        {
            string query = string.Format("SELECT Id, UserId, Email, PrivateKey, PublicKey, CreatedDate, IsActive FROM UserKeys WHERE Email = '{0}' AND IsActive = 'True' LIMIT 1;", _email);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    var datetime = DateTime.ParseExact(reader.GetString(5), "dd/MM/yyyy HH:mm:ss", null);
                    var isActive = (reader.GetString(6) == "True") ? true : false;

                    Models.UserKey userKey = new Models.UserKey(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3),
                                                              reader.GetString(4), datetime, isActive);

                    return userKey;
                }
            }

            return null;
        }

        public bool DeactivateUserKeySet(Models.UserKey _userKey)
        {
            string query = string.Format("UPDATE UserKeys SET IsActive = '{0}' WHERE Id = {1};", false, _userKey.Id);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public bool DeleteUserKeySet(Models.UserKey _userKey)
        {
            string query = string.Format("DELETE FROM UserKeys WHERE Id = {0};", _userKey.Id);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public bool CreatePublicKey(Models.PublicKey _publicKey)
        {
            string query = string.Format("INSERT INTO PublicKeys (UserId, Email, PublicKey, CreatedDate) VALUES ({0}, '{1}', '{2}', '{3}');",
                                         _publicKey.UserId, _publicKey.Email, _publicKey.Key, _publicKey.CreatedDate);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public List<Models.PublicKey> GetPublicKeysByUser(Models.User _user)
        {
            List<Models.PublicKey> set = new List<Models.PublicKey>();

            string query = string.Format("SELECT Id, UserId, Email, PublicKey, CreatedDate FROM PublicKeys WHERE UserId = {0};", _user.Id);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    var datetime = DateTime.ParseExact(reader.GetString(4), "dd/MM/yyyy HH:mm:ss", null);

                    Models.PublicKey model = new Models.PublicKey(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2),
                                                                  reader.GetString(3), datetime);

                    set.Add(model);
                }

                return set;
            }

            return null;
        }

        public Models.PublicKey GetPublicKeyByEmail(string _email)
        {
            string query = string.Format("SELECT Id, UserId, Email, PublicKey, CreatedDate FROM PublicKeys WHERE Email = '{0}' LIMIT 1;", _email);

            SQLiteDataReader reader = ExecuteRead(query);

            if (reader != null)
            {
                while (reader.Read())
                {
                    var datetime = DateTime.ParseExact(reader.GetString(4), "dd/MM/yyyy HH:mm:ss", null);

                    Models.PublicKey pkey = new Models.PublicKey(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2),
                                                                 reader.GetString(3), datetime);
                    return pkey;
                }
            }

            return null;
        }

        public bool DeletePublicKey(Models.PublicKey _publicKey)
        {
            string query = string.Format("DELETE FROM PublicKeys WHERE Id = {0};", _publicKey.Id);

            if (ExecuteWrite(query) == 1)
                return true;

            return false;
        }

        public void Dispose()
        {
            db.Close();
            db.Dispose();
            GC.Collect();
        }
    }
}
