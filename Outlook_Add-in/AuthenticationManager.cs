﻿using ConfusedWeasel.Outlook.KeyStore;
using RestfulAPI;
using RestfulAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocalDbModels = ConfusedWeasel.Outlook.KeyStore.Models;

namespace Outlook_Add_in
{
    public class Auth
    {
        public LocalDbModels.User User { get; set; }
        public AccessToken Token { get; set; }
    }

    public class AuthenticationManager
    {
        private static AuthenticationManager _instance;

        public static AuthenticationManager GetInstance { get { return _instance ?? (_instance = new AuthenticationManager()); } set { GetInstance = value; } }

        public bool IsAuthenticated { get; set; }
        public string ClientId { get; set; }
        public Auth CurrentUser { get; set; }
        public Resources LocalDb { get; set; }

        private AuthenticationManager()
        {
            ClientId = "d622b450-cfac-11e3-9c1a-0800200c9a66";
            LocalDb = Resources.GetInstance;
        }

        public bool AuthenticateUser(string username, string password)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                return false;

            var oauth = new OAuthConsumer();
            try
            {
                var tmpToken = oauth.GetAccessToken(ClientId, username, password);
                if (tmpToken != null)
                {
                    var localUser = GetLocalStoredUser(username);
                    if (localUser == null)
                    {
                        var userData = new UserData();
                        var user = userData.GetUserByAccessToken(tmpToken);
                        if (user != null)
                        {
                            localUser = StoreUserLocally(user);
                        }
                    }

                    CurrentUser = new Auth()
                    {
                        User = localUser,
                        Token = tmpToken
                    };
                    IsAuthenticated = true;
                }

                return IsAuthenticated;
            }
            catch (Exception ex)
            {
                //Log error
                return false;
            }
        }

        public void SignOutUser()
        {
            IsAuthenticated = false;
            CurrentUser = null;
        }

        private LocalDbModels.User GetLocalStoredUser(string userName)
        {
            return LocalDb.GetUserByUserName(userName);
        }

        private LocalDbModels.User StoreUserLocally(User user)
        {
            if (user == null)
                throw new ArgumentNullException("The supplied object of the type User was null");

            return LocalDb.CreateUser(user.ToLocalObject());
        }
    }
}
