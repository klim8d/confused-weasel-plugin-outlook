﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocalDbModels = ConfusedWeasel.Outlook.KeyStore.Models;
using ApiModels = RestfulAPI.Models;

namespace Outlook_Add_in
{
    public static class ExtensionMethods
    {
        public static LocalDbModels.User ToLocalObject(this ApiModels.User user)
        {
            return new LocalDbModels.User
            {
                UserName = user.UserName,
                UserUuid = user.UserUuid
            };
        }

        public static ApiModels.User ToApiModel(this LocalDbModels.User user)
        {
            return new ApiModels.User
            {
                UserName = user.UserName,
                UserUuid = user.UserUuid
            };
        }
    }
}
