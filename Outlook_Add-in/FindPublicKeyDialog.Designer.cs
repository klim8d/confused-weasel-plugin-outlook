﻿namespace Outlook_Add_in
{
    partial class FindPublicKeyDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEmail = new System.Windows.Forms.Label();
            this.txtParameterField = new System.Windows.Forms.TextBox();
            this.buttonLookup = new System.Windows.Forms.Button();
            this.buttonStoreKey = new System.Windows.Forms.Button();
            this.txtPublicKey = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(12, 15);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(35, 13);
            this.labelEmail.TabIndex = 0;
            this.labelEmail.Text = "E-mail";
            // 
            // txtParameterField
            // 
            this.txtParameterField.Location = new System.Drawing.Point(53, 12);
            this.txtParameterField.Name = "txtParameterField";
            this.txtParameterField.Size = new System.Drawing.Size(435, 20);
            this.txtParameterField.TabIndex = 1;
            // 
            // buttonLookup
            // 
            this.buttonLookup.Location = new System.Drawing.Point(494, 11);
            this.buttonLookup.Name = "buttonLookup";
            this.buttonLookup.Size = new System.Drawing.Size(75, 22);
            this.buttonLookup.TabIndex = 2;
            this.buttonLookup.Text = "Lookup";
            this.buttonLookup.UseVisualStyleBackColor = true;
            this.buttonLookup.Click += new System.EventHandler(this.LookupPublicKey);
            // 
            // buttonStoreKey
            // 
            this.buttonStoreKey.Location = new System.Drawing.Point(494, 250);
            this.buttonStoreKey.Name = "buttonStoreKey";
            this.buttonStoreKey.Size = new System.Drawing.Size(75, 22);
            this.buttonStoreKey.TabIndex = 4;
            this.buttonStoreKey.Text = "Store key";
            this.buttonStoreKey.UseVisualStyleBackColor = true;
            this.buttonStoreKey.Click += new System.EventHandler(this.SaveKey);
            // 
            // txtPublicKey
            // 
            this.txtPublicKey.Location = new System.Drawing.Point(15, 39);
            this.txtPublicKey.Multiline = true;
            this.txtPublicKey.Name = "txtPublicKey";
            this.txtPublicKey.ReadOnly = true;
            this.txtPublicKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPublicKey.Size = new System.Drawing.Size(554, 205);
            this.txtPublicKey.TabIndex = 5;
            // 
            // FindPublicKeyDialog
            // 
            this.AcceptButton = this.buttonLookup;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 285);
            this.Controls.Add(this.txtPublicKey);
            this.Controls.Add(this.buttonStoreKey);
            this.Controls.Add(this.buttonLookup);
            this.Controls.Add(this.txtParameterField);
            this.Controls.Add(this.labelEmail);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(597, 324);
            this.MinimumSize = new System.Drawing.Size(597, 324);
            this.Name = "FindPublicKeyDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find public key";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox txtParameterField;
        private System.Windows.Forms.Button buttonLookup;
        private System.Windows.Forms.Button buttonStoreKey;
        private System.Windows.Forms.TextBox txtPublicKey;
    }
}