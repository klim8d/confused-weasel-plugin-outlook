﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using API = RestfulAPI;
using APIModels = RestfulAPI.Models;
using Repository = ConfusedWeasel.Outlook.KeyStore;
using RepositoryModels = ConfusedWeasel.Outlook.KeyStore.Models;

namespace Outlook_Add_in
{
    public partial class FindPublicKeyDialog : Form
    {
        private API.KeyStore keyStoreAPI;
        private APIModels.PGPKey pgpKey;
        private Repository.Resources resources;
        private RepositoryModels.PublicKey publicKey;
        private AuthenticationManager authenticationManager;
        private Action _refreshListCallback;

        public FindPublicKeyDialog()
        {
            keyStoreAPI = new API.KeyStore();
            resources = Repository.Resources.GetInstance;

            authenticationManager = AuthenticationManager.GetInstance;

            InitializeComponent();
        }

        public void ShowUI(Action callback)
        {
            _refreshListCallback = callback;
            this.Show();
        }

        private void LookupPublicKey(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtParameterField.Text))
                {
                    this.KeySearch(txtParameterField.Text);
                }
            }
            catch
            {
                MessageBox.Show("It wasn't possible to fetch any key...", "Find public key - Lookup", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveKey(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtPublicKey.Text))
                {
                    if (pgpKey != null)
                    {
                        DialogResult confirmResult = MessageBox.Show("Are you sure, you want to store the public key for " + pgpKey.Email + "?",
                            "Store public key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (confirmResult == DialogResult.Yes)
                            this.StorePublicKey();
                    }
                }
            }
            catch
            {
                MessageBox.Show("It wasn't possible to store retrieved public key...", "Find public key - Lookup", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KeySearch(string _email)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            Task.Run(async () =>
            {
                pgpKey = keyStoreAPI.GetKeyByEmail(_email);
                if (pgpKey != null)
                {
                    await dispatcher.BeginInvoke(new MethodInvoker(() =>
                    {
                        this.txtPublicKey.Text = pgpKey.Key;
                    }));
                }
                else
                {
                    await dispatcher.BeginInvoke(new MethodInvoker(() =>
                    {
                        MessageBox.Show("The supplied email address (" + _email + "), did not return a result", "Find public key - Lookup", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }));
                }
            });
        }

        private void StorePublicKey()
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            Task.Run(async () =>
            {
                publicKey = new RepositoryModels.PublicKey
                {
                    UserId = authenticationManager.CurrentUser.User.Id,
                    Email = pgpKey.Email,
                    Key = pgpKey.Key,
                    CreatedDate = pgpKey.UpdatedAt.Value,
                };

                if (resources.CreatePublicKey(publicKey) == true)
                    await dispatcher.BeginInvoke(new MethodInvoker(() =>
                    {
                        _refreshListCallback();
                        this.Close();
                    }));
                else
                    await dispatcher.BeginInvoke(new MethodInvoker(() =>
                {
                    MessageBox.Show("The public key wasn't stored...", "Find public key - Lookup", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }));
            });
        }
    }
}
