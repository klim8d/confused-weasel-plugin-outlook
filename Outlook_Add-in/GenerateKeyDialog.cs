﻿using KeyGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Outlook_Add_in
{
    public partial class GenerateKeyDialog : Form
    {
        private AuthenticationManager _authMngr;
        private Action _refreshCallback;

        public GenerateKeyDialog()
        {
            InitializeComponent();
            _authMngr = AuthenticationManager.GetInstance;
        }

        public void ShowUI(Action callback)
        {
            _refreshCallback = callback;
            this.Show();
        }

        private async void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtPassword.Text) || String.IsNullOrEmpty(txtRePassword.Text))
                {
                    MessageBox.Show("Please fill out both fields");
                    return;
                }

                if (!txtPassword.Text.Equals(txtRePassword.Text))
                {
                    MessageBox.Show("Passwords does not match");
                    return;
                }
                if (txtPassword.Text.Length < 8)
                {
                    MessageBox.Show("Passwords must be atleast 8 characters or longer");
                    return;
                }

                var confirm = MessageBox.Show("If a public key already exists for this email, it will be disabled. Are you sure you want to proceed?",
                    "Confirm generate new key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (confirm == DialogResult.Yes)
                {
                    ChangeUIState(false);
                    var privStream = new MemoryStream();
                    var pubStream = new MemoryStream();

                    var keys = await GenerateRsaKey(privStream, pubStream,
                        () =>
                        {
                            pubStream.Position = 0;
                            privStream.Position = 0;
                            string pubString = Encoding.UTF8.GetString(pubStream.ToArray());
                            string privString = Encoding.UTF8.GetString(privStream.ToArray());
                            var keystoreMngr = new KeyStoreManager();
                            
                            var outlookObj = new Outlook.Application();
                            var email = outlookObj.Session.CurrentUser.AddressEntry.Address;

                            bool success = keystoreMngr.StoreNewKeyPair(_authMngr.CurrentUser.User, email, pubString, privString);
                            if (success)
                            {
                                MessageBox.Show("Success");
                                _refreshCallback();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Could not save the keys. Please try again later or contact support", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                ChangeUIState(true);
                            }
                        },
                        () =>
                        {
                            MessageBox.Show("Something went wrong");
                            ChangeUIState(true);
                        });
                }
                else
                {
                    MessageBox.Show("Nothing changed");
                    txtPassword.Text = "";
                    txtRePassword.Text = "";
                    ChangeUIState(true);
                }
            }
            catch (Exception ex)
            {
                //Log exception
                MessageBox.Show("We are sorry, something went wrong. Please try again later.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangeUIState(bool state)
        {
            txtPassword.Enabled = state;
            txtRePassword.Enabled = state;
            btnCreate.Enabled = state;
        }

        private Task<PGPKeyPair> GenerateRsaKey(Stream privStream, Stream pubStream, Action successCallback, Action exceptionCallback)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            return Task<PGPKeyPair>.Run(async () =>
            {
                var rsaKeyGen = new RSAGenerator();
                var keys = rsaKeyGen.GenerateKey(_authMngr.CurrentUser.User.UserName, txtPassword.Text, privStream, pubStream);
                if (keys.PrivateKey != null && keys.PublicKey != null)
                {
                    await dispatcher.BeginInvoke(successCallback);
                    return keys;
                }
                else
                {
                    await dispatcher.BeginInvoke(exceptionCallback);
                    return keys;
                }
            });
        }
    }
}
