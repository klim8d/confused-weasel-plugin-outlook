﻿using ConfusedWeasel.Outlook.KeyStore;
using RestfulAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocalDbModels = ConfusedWeasel.Outlook.KeyStore.Models;

namespace Outlook_Add_in
{
    public class KeyStoreManager
    {
        public Resources LocalDb { get; private set; }
        public KeyStore API { get; private set; }
        public AuthenticationManager AuthMngr { get; set; }

        public KeyStoreManager()
        {
            LocalDb = Resources.GetInstance;
            API = new KeyStore();
            AuthMngr = AuthenticationManager.GetInstance;
        }

        public bool StoreNewKeyPair(LocalDbModels.User user, string email, string publicKey, string privateKey)
        {
            if (user == null || String.IsNullOrEmpty(email) || String.IsNullOrEmpty(publicKey) || String.IsNullOrEmpty(privateKey))
                return false;

            bool success = true;
            using (var transaction = LocalDb.BeginTransaction())
            {
                try
                {
                    var currentKey = LocalDb.GetUserKeySetByEmail(email);
                    if (currentKey != null)
                        success = LocalDb.DeactivateUserKeySet(currentKey);

                    if (success)
                    {
                        success = LocalDb.CreateUserKeySet(new LocalDbModels.UserKey
                        {
                            UserId = user.Id,
                            Email = email,
                            PublicKey = publicKey,
                            PrivateKey = privateKey,
                            IsActive = true,
                            CreatedDate = DateTime.Now
                        });

                        if (success)
                        {
                            success = API.CreateKey(new RestfulAPI.Models.UserKeyObejct
                                {
                                    User = user.ToApiModel(),
                                    PGPKey = new RestfulAPI.Models.PGPKey
                                    {
                                        Email = email,
                                        Key = publicKey
                                    }
                                });
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Log exception
                    return false;
                }

                if (success)
                    transaction.Commit();
                else
                    transaction.Rollback();
            }

            return success;
        }
    }
}
