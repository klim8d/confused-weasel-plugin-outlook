﻿namespace Outlook_Add_in
{
    partial class KeyStoreWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabKeys = new System.Windows.Forms.TabControl();
            this.tabOwnKeysets = new System.Windows.Forms.TabPage();
            this.dataOwnKeySets = new System.Windows.Forms.DataGridView();
            this.tabPublicKeys = new System.Windows.Forms.TabPage();
            this.dataPublicKeys = new System.Windows.Forms.DataGridView();
            this.buttonNewKeyset = new System.Windows.Forms.Button();
            this.buttonDisableKeyset = new System.Windows.Forms.Button();
            this.buttonFindPublicKey = new System.Windows.Forms.Button();
            this.buttonDeletePublicKey = new System.Windows.Forms.Button();
            this.buttonViewKeySet = new System.Windows.Forms.Button();
            this.tabKeys.SuspendLayout();
            this.tabOwnKeysets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataOwnKeySets)).BeginInit();
            this.tabPublicKeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataPublicKeys)).BeginInit();
            this.SuspendLayout();
            // 
            // tabKeys
            // 
            this.tabKeys.Controls.Add(this.tabOwnKeysets);
            this.tabKeys.Controls.Add(this.tabPublicKeys);
            this.tabKeys.Location = new System.Drawing.Point(12, 12);
            this.tabKeys.Name = "tabKeys";
            this.tabKeys.SelectedIndex = 0;
            this.tabKeys.Size = new System.Drawing.Size(475, 254);
            this.tabKeys.TabIndex = 0;
            this.tabKeys.SelectedIndexChanged += new System.EventHandler(this.TabIndexChange);
            // 
            // tabOwnKeysets
            // 
            this.tabOwnKeysets.Controls.Add(this.dataOwnKeySets);
            this.tabOwnKeysets.Location = new System.Drawing.Point(4, 22);
            this.tabOwnKeysets.Name = "tabOwnKeysets";
            this.tabOwnKeysets.Padding = new System.Windows.Forms.Padding(3);
            this.tabOwnKeysets.Size = new System.Drawing.Size(467, 228);
            this.tabOwnKeysets.TabIndex = 0;
            this.tabOwnKeysets.Text = "Own keysets";
            this.tabOwnKeysets.UseVisualStyleBackColor = true;
            // 
            // dataOwnKeySets
            // 
            this.dataOwnKeySets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataOwnKeySets.Location = new System.Drawing.Point(1, 1);
            this.dataOwnKeySets.Name = "dataOwnKeySets";
            this.dataOwnKeySets.Size = new System.Drawing.Size(465, 226);
            this.dataOwnKeySets.TabIndex = 0;
            this.dataOwnKeySets.SelectionChanged += new System.EventHandler(this.SelectionChangedInKeySets);
            // 
            // tabPublicKeys
            // 
            this.tabPublicKeys.Controls.Add(this.dataPublicKeys);
            this.tabPublicKeys.Location = new System.Drawing.Point(4, 22);
            this.tabPublicKeys.Name = "tabPublicKeys";
            this.tabPublicKeys.Padding = new System.Windows.Forms.Padding(3);
            this.tabPublicKeys.Size = new System.Drawing.Size(467, 228);
            this.tabPublicKeys.TabIndex = 1;
            this.tabPublicKeys.Text = "Public keys";
            this.tabPublicKeys.UseVisualStyleBackColor = true;
            // 
            // dataPublicKeys
            // 
            this.dataPublicKeys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataPublicKeys.Location = new System.Drawing.Point(1, 1);
            this.dataPublicKeys.Name = "dataPublicKeys";
            this.dataPublicKeys.Size = new System.Drawing.Size(465, 226);
            this.dataPublicKeys.TabIndex = 0;
            // 
            // buttonNewKeyset
            // 
            this.buttonNewKeyset.Location = new System.Drawing.Point(494, 34);
            this.buttonNewKeyset.Name = "buttonNewKeyset";
            this.buttonNewKeyset.Size = new System.Drawing.Size(131, 23);
            this.buttonNewKeyset.TabIndex = 1;
            this.buttonNewKeyset.Text = "Generate new keyset";
            this.buttonNewKeyset.UseVisualStyleBackColor = true;
            this.buttonNewKeyset.Click += new System.EventHandler(this.OpenCreateNewKeySet);
            // 
            // buttonDisableKeyset
            // 
            this.buttonDisableKeyset.Enabled = false;
            this.buttonDisableKeyset.Location = new System.Drawing.Point(494, 92);
            this.buttonDisableKeyset.Name = "buttonDisableKeyset";
            this.buttonDisableKeyset.Size = new System.Drawing.Size(131, 23);
            this.buttonDisableKeyset.TabIndex = 2;
            this.buttonDisableKeyset.Text = "Disable selected keyset";
            this.buttonDisableKeyset.UseVisualStyleBackColor = true;
            this.buttonDisableKeyset.Click += new System.EventHandler(this.DisableKeySet);
            // 
            // buttonFindPublicKey
            // 
            this.buttonFindPublicKey.Location = new System.Drawing.Point(494, 34);
            this.buttonFindPublicKey.Name = "buttonFindPublicKey";
            this.buttonFindPublicKey.Size = new System.Drawing.Size(131, 23);
            this.buttonFindPublicKey.TabIndex = 3;
            this.buttonFindPublicKey.Text = "Find public key";
            this.buttonFindPublicKey.UseVisualStyleBackColor = true;
            this.buttonFindPublicKey.Visible = false;
            this.buttonFindPublicKey.Click += new System.EventHandler(this.OpenFindPublicKey);
            // 
            // buttonDeletePublicKey
            // 
            this.buttonDeletePublicKey.Location = new System.Drawing.Point(494, 92);
            this.buttonDeletePublicKey.Name = "buttonDeletePublicKey";
            this.buttonDeletePublicKey.Size = new System.Drawing.Size(131, 23);
            this.buttonDeletePublicKey.TabIndex = 4;
            this.buttonDeletePublicKey.Text = "Delete public key";
            this.buttonDeletePublicKey.UseVisualStyleBackColor = true;
            this.buttonDeletePublicKey.Visible = false;
            this.buttonDeletePublicKey.Click += new System.EventHandler(this.DeletePublicKey);
            // 
            // buttonViewKeySet
            // 
            this.buttonViewKeySet.Location = new System.Drawing.Point(493, 63);
            this.buttonViewKeySet.Name = "buttonViewKeySet";
            this.buttonViewKeySet.Size = new System.Drawing.Size(131, 23);
            this.buttonViewKeySet.TabIndex = 5;
            this.buttonViewKeySet.Text = "View key(s)";
            this.buttonViewKeySet.UseVisualStyleBackColor = true;
            this.buttonViewKeySet.Click += new System.EventHandler(this.OpenKeyViewer);
            // 
            // KeyStoreWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 282);
            this.Controls.Add(this.buttonViewKeySet);
            this.Controls.Add(this.buttonDeletePublicKey);
            this.Controls.Add(this.buttonFindPublicKey);
            this.Controls.Add(this.buttonDisableKeyset);
            this.Controls.Add(this.buttonNewKeyset);
            this.Controls.Add(this.tabKeys);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(651, 321);
            this.MinimumSize = new System.Drawing.Size(651, 321);
            this.Name = "KeyStoreWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Keystore Manager";
            this.tabKeys.ResumeLayout(false);
            this.tabOwnKeysets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataOwnKeySets)).EndInit();
            this.tabPublicKeys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataPublicKeys)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabKeys;
        private System.Windows.Forms.TabPage tabOwnKeysets;
        private System.Windows.Forms.TabPage tabPublicKeys;
        private System.Windows.Forms.Button buttonNewKeyset;
        private System.Windows.Forms.Button buttonDisableKeyset;
        private System.Windows.Forms.DataGridView dataOwnKeySets;
        private System.Windows.Forms.DataGridView dataPublicKeys;
        private System.Windows.Forms.Button buttonFindPublicKey;
        private System.Windows.Forms.Button buttonDeletePublicKey;
        private System.Windows.Forms.Button buttonViewKeySet;
    }
}