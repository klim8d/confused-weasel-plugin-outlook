﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Repository = ConfusedWeasel.Outlook.KeyStore;
using Model = ConfusedWeasel.Outlook.KeyStore.Models;
using API = RestfulAPI;
using APIModel = RestfulAPI.Models;
using System.Windows.Threading;

namespace Outlook_Add_in
{
    public partial class KeyStoreWindow : Form
    {
        private AuthenticationManager authenticationManager;
        private Repository.Resources resources;
        private API.KeyStore keyStoreAPI;
        private APIModel.PartialKey partialKey;

        private int currentView = 0;

        public KeyStoreWindow()
        {
            authenticationManager = AuthenticationManager.GetInstance;
            resources = Repository.Resources.GetInstance;
            keyStoreAPI = new API.KeyStore();

            InitializeComponent();

            PopulateUserKeys();
            PopulatePublicKeys();
        }

        private void PopulateUserKeys()
        {
            RefreshUserKeys();

            this.dataOwnKeySets.ReadOnly = true;

            this.dataOwnKeySets.Columns[0].Visible = false;
            this.dataOwnKeySets.Columns[1].Visible = false;
            this.dataOwnKeySets.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataOwnKeySets.Columns[2].HeaderText = "E-mail";
            this.dataOwnKeySets.Columns[3].Visible = false;
            this.dataOwnKeySets.Columns[4].Visible = false;
            this.dataOwnKeySets.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataOwnKeySets.Columns[5].HeaderText = "Created date";
            this.dataOwnKeySets.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.dataOwnKeySets.Columns[6].HeaderText = "Is active";
        }

        private void RefreshUserKeys()
        {
            BindingList<Model.UserKey> userKeys = new BindingList<Model.UserKey>(resources.GetUserKeySetsByUser(authenticationManager.CurrentUser.User));
            BindingSource userKeysSource = new BindingSource(userKeys, null);

            this.dataOwnKeySets.DataSource = userKeysSource;
        }

        private void PopulatePublicKeys()
        {
            RefreshPublicKeys();

            this.dataPublicKeys.ReadOnly = true;

            this.dataPublicKeys.Columns[0].Visible = false;
            this.dataPublicKeys.Columns[1].Visible = false;
            this.dataPublicKeys.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataPublicKeys.Columns[2].HeaderText = "E-mail";
            this.dataPublicKeys.Columns[3].Visible = false;
            this.dataPublicKeys.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dataPublicKeys.Columns[4].HeaderText = "Created date";
        }

        private void RefreshPublicKeys()
        {
            BindingList<Model.PublicKey> publicKeys = new BindingList<Model.PublicKey>(resources.GetPublicKeysByUser(authenticationManager.CurrentUser.User));
            BindingSource publicKeysSource = new BindingSource(publicKeys, null);

            this.dataPublicKeys.DataSource = publicKeysSource;
        }

        private void TabIndexChange(object sender, EventArgs e)
        {
            currentView = (sender as TabControl).SelectedIndex;

            if (currentView == 0)
            {
                this.buttonFindPublicKey.Visible = false;
                this.buttonDeletePublicKey.Visible = false;

                this.buttonNewKeyset.Visible = true;
                this.buttonDisableKeyset.Visible = true;
            }
            else
            {
                this.buttonNewKeyset.Visible = false;
                this.buttonDisableKeyset.Visible = false;

                this.buttonFindPublicKey.Visible = true;
                this.buttonDeletePublicKey.Visible = true;
            }
        }

        private void OpenCreateNewKeySet(object sender, EventArgs e)
        {
            GenerateKeyDialog form = new GenerateKeyDialog();
            form.ShowUI(RefreshUserKeys);
        }

        private void DisableKeySet(object sender, EventArgs e)
        {
            string keySetOwner = dataOwnKeySets.Rows[dataOwnKeySets.CurrentCell.RowIndex].Cells[2].Value.ToString();

            DialogResult confirmResult = MessageBox.Show("Are you sure, you want to disable the keyset for " + keySetOwner + "?",
                "Store public key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (confirmResult == DialogResult.Yes)
            {
                try
                {
                    buttonDisableKeyset.Enabled = false;
                    DeactivateKeySet(keySetOwner);
                }
                catch (Exception ex)
                {
                    //Log ex
                    MessageBox.Show("Something went wrong. We were unable to disable the keyset.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void DeactivateKeySet(string owner)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            Task.Run(async () =>
            {
                bool result = resources.DeactivateUserKeySet(resources.GetUserKeySetByEmail(owner));

                if (result == true)
                {
                    partialKey = new APIModel.PartialKey
                    {
                        Email = owner,
                    };

                    bool success = keyStoreAPI.DeleteKey(partialKey);
                    if (success)
                    {
                        await dispatcher.BeginInvoke(new MethodInvoker(() =>
                        {
                            RefreshUserKeys();
                            buttonDisableKeyset.Enabled = true;
                        }));
                    }
                    else
                    {
                        await dispatcher.BeginInvoke(new MethodInvoker(() =>
                        {
                            MessageBox.Show("Something went wrong. We were unable to disable the keyset.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            buttonDisableKeyset.Enabled = true;
                        }));
                    }
                }
            });
        }

        private void OpenFindPublicKey(object sender, EventArgs e)
        {
            FindPublicKeyDialog form = new FindPublicKeyDialog();
            form.ShowUI(RefreshPublicKeys);
        }

        public void DeletePublicKey(object sender, EventArgs e)
        {
            string PublicKeyOwner = dataPublicKeys.Rows[dataPublicKeys.CurrentCell.RowIndex].Cells[2].Value.ToString();

            DialogResult confirmResult = MessageBox.Show("Are you sure, you want to delete the public key for " + PublicKeyOwner + "?",
                "Delete public key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (confirmResult == DialogResult.Yes)
            {
                try
                {
                    bool result = resources.DeletePublicKey(resources.GetPublicKeyByEmail(PublicKeyOwner));
                    RefreshPublicKeys();
                }
                catch (Exception ex)
                {
                    //Log ex
                    MessageBox.Show("Something went wrong. We were unable to delete the publickey.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void OpenKeyViewer(object sender, EventArgs e)
        {
            KeyViewDialog form;

            if (currentView == 0)
            {
                try
                {
                    string privatekey = dataOwnKeySets.Rows[dataOwnKeySets.CurrentCell.RowIndex].Cells[3].Value.ToString();
                    string publickey = dataOwnKeySets.Rows[dataOwnKeySets.CurrentCell.RowIndex].Cells[4].Value.ToString();

                    form = new KeyViewDialog("UserKeySet", privatekey, publickey);
                    form.Show();
                }
                catch
                {
                    MessageBox.Show("It wasn't possible to open the selected keyset.", "View key(s)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                try
                {
                    string publickey = dataPublicKeys.Rows[dataPublicKeys.CurrentCell.RowIndex].Cells[3].Value.ToString();

                    form = new KeyViewDialog("PublicKey", publickey);
                    form.Show();
                }
                catch
                {
                    MessageBox.Show("It wasn't possible to open the selected public key.", "View key(s)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void SelectionChangedInKeySets(object sender, EventArgs e)
        {
            bool rowState = Convert.ToBoolean(dataOwnKeySets.Rows[dataOwnKeySets.CurrentCell.RowIndex].Cells[6].Value.ToString());

            if (rowState == true)
                buttonDisableKeyset.Enabled = true;
            else
                buttonDisableKeyset.Enabled = false;
        }
    }
}
