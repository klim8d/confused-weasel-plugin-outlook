﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Outlook_Add_in
{
    public partial class KeyViewDialog : Form
    {
        public KeyViewDialog(string _type, string _publicKey)
        {
            InitializeComponent();

            if (_type == "PublicKey")
                SetupPublicKeyView(_publicKey);
        }

        public KeyViewDialog(string _type, string _privateKey, string _publicKey)
        {
            InitializeComponent();

            if (_type == "UserKeySet")
                SetupKeySetView(_privateKey, _publicKey);
        }

        private void SetupKeySetView(string _privateKey, string _publicKey)
        {
            this.txtPrivateKey.Text = _privateKey;
            this.txtPublicKey.Text = _publicKey;
        }

        private void SetupPublicKeyView(string _publicKey)
        {
            this.ClientSize = new System.Drawing.Size(553, 254);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(553, 254);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(553, 254);

            this.labelPrivateKey.Visible = false;
            this.txtPrivateKey.Visible = false;
            this.labelPublicKey.Location = new System.Drawing.Point(9, 8);
            this.txtPublicKey.Location = new System.Drawing.Point(12, 25);

            this.txtPublicKey.Text = _publicKey;
        }
    }
}
