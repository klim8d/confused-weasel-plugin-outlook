﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace Outlook_Add_in
{
    public partial class LoginDialog : Form
    {
        private AuthenticationManager _comm;
        private Action _callbackEvent;

        public LoginDialog()
        {
            InitializeComponent();
            _comm = AuthenticationManager.GetInstance;
            txtUserName.Text = "JohnDoe"; //For debugging purposes
            txtPassword.Text = "ThisIsASuperSecurePassword"; //For debugging purposes
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtUserName.Text) || String.IsNullOrEmpty(txtPassword.Text))
                    MessageBox.Show("Both fields are required", "Validation message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ChangeUIState(false);
                AuthenticateUser(txtUserName.Text, txtPassword.Text, () =>
                {
                    _callbackEvent();
                    this.Close();
                },
                () =>
                {
                    MessageBox.Show("Could not login. Wrong username or password.", "Wrong username or password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ChangeUIState(true);
                });
            }
            catch (Exception ex)
            {
                //Log exception
                MessageBox.Show("We are sorry, something went wrong. Please try again later.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ChangeUIState(bool state)
        {
            txtUserName.Enabled = state;
            txtPassword.Enabled = state;
            btnLogin.Enabled = state;
        }

        private Task<bool> AuthenticateUser(string userName, string password, Action successCallback, Action exceptionCallback)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            return Task<bool>.Run(async () =>
            {
                bool success = _comm.AuthenticateUser(txtUserName.Text, txtPassword.Text);
                if (success)
                    await dispatcher.BeginInvoke(successCallback);
                else
                    await dispatcher.BeginInvoke(exceptionCallback);

                return success;
            });
        }

        public void ShowDialog(Action callback)
        {
            this.Show();
            _callbackEvent = callback;
        }
    }
}
