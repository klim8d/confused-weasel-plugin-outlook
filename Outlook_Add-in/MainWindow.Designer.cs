﻿using System;
namespace Outlook_Add_in
{
    partial class MainWindow : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public MainWindow()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpLogin = this.Factory.CreateRibbonGroup();
            this.lblAuthStatus = this.Factory.CreateRibbonLabel();
            this.btnOpenLogin = this.Factory.CreateRibbonButton();
            this.grpUser = this.Factory.CreateRibbonGroup();
            this.lblUserName = this.Factory.CreateRibbonLabel();
            this.buttonGroup1 = this.Factory.CreateRibbonButtonGroup();
            this.buttonKeyStoreManager = this.Factory.CreateRibbonButton();
            this.btnSignOut = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpLogin.SuspendLayout();
            this.grpUser.SuspendLayout();
            this.buttonGroup1.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpLogin);
            this.tab1.Groups.Add(this.grpUser);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // grpLogin
            // 
            this.grpLogin.Items.Add(this.lblAuthStatus);
            this.grpLogin.Items.Add(this.btnOpenLogin);
            this.grpLogin.Label = "Login";
            this.grpLogin.Name = "grpLogin";
            // 
            // lblAuthStatus
            // 
            this.lblAuthStatus.Label = "To use this add in, \r\nplease login to the system";
            this.lblAuthStatus.Name = "lblAuthStatus";
            // 
            // btnOpenLogin
            // 
            this.btnOpenLogin.Label = "Open login window";
            this.btnOpenLogin.Name = "btnOpenLogin";
            this.btnOpenLogin.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnOpenLogin_Click);
            // 
            // grpUser
            // 
            this.grpUser.Items.Add(this.lblUserName);
            this.grpUser.Items.Add(this.buttonGroup1);
            this.grpUser.Items.Add(this.btnSignOut);
            this.grpUser.Label = "User";
            this.grpUser.Name = "grpUser";
            this.grpUser.Visible = false;
            // 
            // lblUserName
            // 
            this.lblUserName.Label = "label1";
            this.lblUserName.Name = "lblUserName";
            // 
            // buttonGroup1
            // 
            this.buttonGroup1.Items.Add(this.buttonKeyStoreManager);
            this.buttonGroup1.Name = "buttonGroup1";
            // 
            // buttonKeyStoreManager
            // 
            this.buttonKeyStoreManager.Label = "Keystore Manager";
            this.buttonKeyStoreManager.Name = "buttonKeyStoreManager";
            this.buttonKeyStoreManager.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.OpenKeyStoreManager);
            // 
            // btnSignOut
            // 
            this.btnSignOut.Label = "Sign out";
            this.btnSignOut.Name = "btnSignOut";
            this.btnSignOut.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSignOut_Click);
            // 
            // MainWindow
            // 
            this.Name = "MainWindow";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.MainWindow_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpLogin.ResumeLayout(false);
            this.grpLogin.PerformLayout();
            this.grpUser.ResumeLayout(false);
            this.grpUser.PerformLayout();
            this.buttonGroup1.ResumeLayout(false);
            this.buttonGroup1.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpLogin;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblAuthStatus;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnOpenLogin;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpUser;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblUserName;
        internal Microsoft.Office.Tools.Ribbon.RibbonButtonGroup buttonGroup1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonKeyStoreManager;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSignOut;
    }

    partial class ThisRibbonCollection
    {
        internal MainWindow MainWindow
        {
            get { return this.GetRibbon<MainWindow>(); }
        }
    }
}
