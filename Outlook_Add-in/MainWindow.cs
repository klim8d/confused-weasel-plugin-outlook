﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;

namespace Outlook_Add_in
{
    public partial class MainWindow
    {
        private AuthenticationManager _auth;

        private void MainWindow_Load(object sender, RibbonUIEventArgs e)
        {
            _auth = AuthenticationManager.GetInstance;
            grpLogin.Visible = !_auth.IsAuthenticated;
        }

        private void btnOpenLogin_Click(object sender, RibbonControlEventArgs e)
        {
            var loginForm = new LoginDialog();
            loginForm.ShowDialog(RefreshUI);
        }

        public void RefreshUI()
        {
            if (_auth.IsAuthenticated)
            {
                grpLogin.Visible = false;
                GroupUserLoad(true);
            }
            else
            {
                grpLogin.Visible = true;
                GroupUserLoad(false);
            }
        }

        private void GroupUserLoad(bool isVisible)
        {
            if(isVisible)
                lblUserName.Label = _auth.CurrentUser.User.UserName;
            grpUser.Visible = isVisible;
        }

        private void OpenKeyStoreManager(object sender, RibbonControlEventArgs e)
        {
            KeyStoreWindow form = new KeyStoreWindow();
            form.Show();
        }

        private void btnSignOut_Click(object sender, RibbonControlEventArgs e)
        {
            _auth.SignOutUser();
            RefreshUI();
        }
    }
}
