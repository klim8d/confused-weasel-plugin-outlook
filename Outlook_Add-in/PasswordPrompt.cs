﻿using System;
using System.Windows.Forms;

namespace Outlook_Add_in
{
    public static class PasswordPrompt
    {
        public static string ShowDialog(string _recipientAddress)
        {
            // (PasswordPrompt) Password prompt frame, initial configuration
            Form passwordPrompt = new Form();
            passwordPrompt.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            passwordPrompt.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            passwordPrompt.ClientSize = new System.Drawing.Size(360, 182);
            passwordPrompt.MaximumSize = new System.Drawing.Size(360, 182);
            passwordPrompt.MinimumSize = new System.Drawing.Size(360, 182);
            passwordPrompt.ShowIcon = false;
            passwordPrompt.MaximizeBox = false;
            passwordPrompt.MinimizeBox = false;
            passwordPrompt.ShowInTaskbar = false;
            passwordPrompt.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            passwordPrompt.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            passwordPrompt.Text = "Password prompt";

            // (Label) Form message
            Label labelMessage = new System.Windows.Forms.Label();
            labelMessage.AutoSize = false;
            labelMessage.Location = new System.Drawing.Point(12, 9);
            labelMessage.Size = new System.Drawing.Size(320, 50);
            labelMessage.TabIndex = 0;
            labelMessage.Text = String.Format("The protected message requires the recipient ({0}) to authenticate,\r\nbefore the message body can be decrypted.", _recipientAddress);

            // (Label) Seperator line
            Label labelSeperatorLine = new System.Windows.Forms.Label();
            labelSeperatorLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            labelSeperatorLine.Location = new System.Drawing.Point(12, 75);
            labelSeperatorLine.Size = new System.Drawing.Size(320, 2);
            labelSeperatorLine.TabIndex = 1;

            // (Label) Password label 
            Label labelPassword = new System.Windows.Forms.Label();
            labelPassword.AutoSize = true;
            labelPassword.Location = new System.Drawing.Point(12, 84);
            labelPassword.Size = new System.Drawing.Size(56, 13);
            labelPassword.TabIndex = 2;
            labelPassword.Text = "Password:";

            // (TextBox) Password field
            TextBox textPassword = new System.Windows.Forms.TextBox();
            textPassword.Location = new System.Drawing.Point(71, 81);
            textPassword.Size = new System.Drawing.Size(261, 20);
            textPassword.TabIndex = 3;
            textPassword.UseSystemPasswordChar = true;

            // (Button) OK/Close button
            Button buttonOkay = new System.Windows.Forms.Button();
            buttonOkay.Location = new System.Drawing.Point(256, 108);
            buttonOkay.Size = new System.Drawing.Size(75, 23);
            buttonOkay.TabIndex = 4;
            buttonOkay.Text = "OK/Close";
            buttonOkay.UseVisualStyleBackColor = true;
            buttonOkay.Click += (sender, e) => { passwordPrompt.Close(); };

            // (PasswordPrompt) Password prompt frame, adding controls to frame
            passwordPrompt.Controls.Add(labelMessage);
            passwordPrompt.Controls.Add(labelSeperatorLine);
            passwordPrompt.Controls.Add(labelPassword);
            passwordPrompt.Controls.Add(textPassword);
            passwordPrompt.Controls.Add(buttonOkay);

            // (PasswordPrompt) Password prompt frame, show frame
            passwordPrompt.ShowDialog();

            return textPassword.Text;
        }
    }
}
