﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using ConfusedWeasel.Outlook.KeyStore;
using System.IO;
using KeyGenerator;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using RestfulAPI;
using ConfusedWeasel.Outlook.KeyStore.Models;
using RestfulAPI.Models;
using System.Windows.Forms;

namespace Outlook_Add_in
{
    public partial class ThisAddIn
    {
        Outlook.Inspectors inspectors;
        Outlook.Explorer explore;
        Outlook.Application application;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            ((Outlook.ApplicationEvents_11_Event)Application).Quit += new Outlook.ApplicationEvents_11_QuitEventHandler(ThisAddIn_Quit);
            // New item creation           
            //inspectors = this.Application.Inspectors;
            //inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);

            //// Selection change
            explore = this.Application.ActiveExplorer();
            explore.SelectionChange += new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(Explore_SelectionChange);

            // On send mail event
            application = this.Application;
            application.ItemSend += new Outlook.ApplicationEvents_11_ItemSendEventHandler(Application_ItemSend);
        }

        private void ThisAddIn_Quit()
        {
            Resources.GetInstance.Dispose();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        //void Inspectors_NewInspector(Outlook.Inspector Inspector)
        //{
        //    Outlook.MailItem mailItem = Inspector.CurrentItem as Outlook.MailItem;

        //    if (mailItem != null)
        //    {
        //        if (AuthenticationManager.GetInstance.IsAuthenticated)
        //        {
        //            var recipients = mailItem.Recipients;
        //            foreach (var recipient in recipients)
        //            {

        //            }
        //        }
        //    }
        //}

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool PostMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);
        internal const int WM_CLOSE = 0x10;
        internal static int PostMessageSafe(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
        {
            int ex = 0;
            bool retVal = PostMessage(hWnd, msg, wParam, lParam);
            if (!retVal)
            {
                // error occurred
                ex = Marshal.GetLastWin32Error();
            }

            return ex;
        }

        void Application_ItemSend(object Item, ref bool Cancel)
        {
            try
            {
                Outlook.MailItem mailItem = Item as Outlook.MailItem;

                if (mailItem != null)
                {
                    if (AuthenticationManager.GetInstance.IsAuthenticated)
                    {
                        var encryptedContent = new Dictionary<string, string>(); //Format: {email addr, body}
                        var recipients = mailItem.Recipients;
                        bool sendEncrypted = true; //if a recipient is not found; prompt the user if the mail should be sendt unencrypted;
                        foreach (Outlook.Recipient recipient in recipients)
                        {
                            var reci = recipient;
                            string addr = reci.Address;
                            string publicKey = GetPGPKey(addr);
                            if (String.IsNullOrEmpty(publicKey))
                            {
                                var confirm = MessageBox.Show("Could not publickey for recipient: " + addr + ". Would you like to send this email unencrypted?", "Publickey not found", MessageBoxButtons.YesNo);
                                if (confirm == DialogResult.Yes)
                                {
                                    sendEncrypted = false;
                                    break;
                                }
                                else
                                {
                                    Cancel = true;
                                    return;
                                }
                            }

                            string encryptedBody = EncryptBody(mailItem.Body, publicKey);
                            encryptedContent.Add(addr, encryptedBody);
                        }

                        if (sendEncrypted)
                        {
                            Outlook.MailItem sentMail = (Outlook.MailItem)this.Application.CreateItem(Outlook.OlItemType.olMailItem);

                            foreach (var recipient in encryptedContent)
                            {
                                Outlook.MailItem oMsg = (Outlook.MailItem)application.CreateItem(Outlook.OlItemType.olMailItem);
                                oMsg.Recipients.Add(recipient.Key);
                                //sentMail.Recipients.Add(recipient.Key);
                                oMsg.Subject = mailItem.Subject;
                                oMsg.Body = recipient.Value;
                                oMsg.DeleteAfterSubmit = true;
                                oMsg.Send();
                            }

                            string senderAddr = application.Session.CurrentUser.AddressEntry.Address; ;
                            string publicKey = GetPGPKey(senderAddr);
                            if (!String.IsNullOrEmpty(publicKey))
                            {
                                sentMail.Body = EncryptBody(mailItem.Body, publicKey);
                                sentMail.Subject = mailItem.Subject;
                                string filename = sentMail.Subject;
                                string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

                                foreach (char c in invalid)
                                    filename = filename.Replace(c.ToString(), "");

                                String savepath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + filename + ".msg";
                                sentMail.SaveAs(savepath);
                            }

                            Cancel = true;
                            mailItem.Subject = null;
                            mailItem.Body = null;
                            //Use Windows API to send a CLOSE message to the Inspector window
                            IntPtr _inspHwnd = FindWindow("rctrl_renwnd32", mailItem.GetInspector.Caption);
                            int retVal = PostMessageSafe(_inspHwnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                            //Dicard inspectors text (mail form)
                            mailItem.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log ex
                MessageBox.Show("We are sorry, an error occured and we were unable to send the message. Contact support or try again later.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string EncryptBody(string body, string publicKey)
        {
            string encryptedBody = "";
            using (var stream = publicKey.Streamify())
            {
                var key = stream.ImportPublicKey();
                using (var clearStream = body.Streamify())
                using (var cryptoStream = new MemoryStream())
                {
                    clearStream.PgpEncrypt(cryptoStream, key);
                    cryptoStream.Position = 0;
                    encryptedBody = cryptoStream.Stringify();
                }
            }

            return encryptedBody;
        }

        private string GetPGPKey(string addr)
        {
            var localDb = Resources.GetInstance;
            var publicKey = (PublicKey)localDb.GetPublicKeyByEmail(addr);
            PGPKey pgpKey = null;
            if (publicKey == null) //Try to retrieve public key from API
            {
                var keystore = new KeyStore();
                pgpKey = keystore.GetKeyByEmail(addr);
                if (pgpKey != null)
                {
                    localDb.CreatePublicKey(new PublicKey
                    {
                        UserId = AuthenticationManager.GetInstance.CurrentUser.User.Id,
                        Email = addr,
                        Key = pgpKey.Key,
                        CreatedDate = DateTime.Now
                    });
                }

                return pgpKey != null ? pgpKey.Key : "";
            }

            return publicKey.Key;
        }

        private void Explore_SelectionChange()
        {
            try
            {
                Outlook.Selection selection = explore.Selection;

                if (selection.Count > 0)
                {
                    object selectedItem = selection[1];
                    Outlook.MailItem mailItem = selectedItem as Outlook.MailItem;

                    if (mailItem != null)
                    {
                        Match match = Regex.Match(mailItem.Body, @"^(\D....BEGIN PGP MESSAGE.....)|(\D....END PGP MESSAGE.....\s)$");

                        if (match.Success)
                        {
                            Resources localDb = Resources.GetInstance;
                            Stream outputStream = null;

                            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(mailItem.Body)))
                            {
                                Outlook.Recipients recipients = mailItem.Recipients;

                                foreach (Outlook.Recipient recipient in recipients)
                                {
                                    string password = PasswordPrompt.ShowDialog(recipient.Address);

                                    if (!String.IsNullOrEmpty(password) && !String.IsNullOrWhiteSpace(password))
                                    {
                                        var userKeySet = localDb.GetUserKeySetByEmail(recipient.Address);

                                        outputStream = stream.PgpDecrypt(userKeySet.PrivateKey, password);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Password cannot be null or whitespace.", "Message decryption", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }

                            if (outputStream != null)
                                mailItem.Body = outputStream.Stringify();
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("It wasn't possible to decrypt the message, do to erroneous password or another unknown factor.", "Message decryption", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
