﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace Spike_Outlook_Add_in
{
    public partial class ThisAddIn
    {
        Outlook.Inspectors inspectors;
        Outlook.Explorer explore;
        Outlook.Application application;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            // New item creation           
            inspectors = this.Application.Inspectors;
            inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);
            
            // Selection change
            explore = this.Application.ActiveExplorer();
            explore.SelectionChange += new Outlook.ExplorerEvents_10_SelectionChangeEventHandler(Explore_SelectionChange);

            // On send mail event
            application = this.Application;
            application.ItemSend += new Outlook.ApplicationEvents_11_ItemSendEventHandler(Application_ItemSend);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        void Inspectors_NewInspector(Outlook.Inspector Inspector)
        {
            Outlook.MailItem mailItem = Inspector.CurrentItem as Outlook.MailItem;

            if (mailItem != null)
            {
                if (mailItem.EntryID == null)
                {
                    // http://msdn.microsoft.com/en-us/library/microsoft.office.interop.outlook.mailitem_properties.aspx
                }
            }
        }

        void Explore_SelectionChange()
        {
            Outlook.Selection selection = explore.Selection;

            if (selection.Count > 0)
            {
                object selectedItem = selection[1];
                Outlook.MailItem mailItem = selectedItem as Outlook.MailItem;

                if(mailItem != null)
                {
                    // Note that mail items whos elements has been changed, will also be saved locally.
                    // http://msdn.microsoft.com/en-us/library/microsoft.office.interop.outlook.mailitem_properties.aspx
                }
            }
        }

        void Application_ItemSend(object Item, ref bool Cancel)
        {
            Outlook.MailItem mailItem = Item as Outlook.MailItem;

            if(mailItem != null)
            {
                // http://msdn.microsoft.com/en-us/library/microsoft.office.interop.outlook.mailitem_properties.aspx
            }
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
