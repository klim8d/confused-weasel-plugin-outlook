﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConfusedWeasel.Outlook.KeyStore;
using ConfusedWeasel.Outlook.KeyStore.Models;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class KeyStore
    {
        Resources resources;

        public KeyStore()
        {
            resources = Resources.GetInstance;
        }

        [TestMethod]
        public void T01_CreateUser()
        {
            User user = new User
            {
                UserUuid = Guid.NewGuid().ToString(),
                UserName = "TestUser",
            };

            try
            {
                Assert.IsNotNull(resources.CreateUser(user));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T02_CreateUserKeySet()
        {
            UserKey userKey = new UserKey
            {
                UserId = resources.GetUserByUserName("TestUser").Id,
                Email = "testuser@centoso.com",
                PrivateKey = "private_pgpkey",
                PublicKey = "public_pgpkey",
                CreatedDate = DateTime.UtcNow,
                IsActive = true,
            };

            try
            {
                Assert.IsTrue(resources.CreateUserKeySet(userKey));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T03_CreatePublicKey()
        {
            PublicKey publicKey = new PublicKey
            {
                UserId = resources.GetUserByUserName("TestUser").Id,
                Email = "pgp-public@centoso.com",
                Key = "public_pgpkey",
                CreatedDate = DateTime.UtcNow,
            };

            try
            {
                Assert.IsTrue(resources.CreatePublicKey(publicKey));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T04_GetUserById()
        {
            try
            {
                Assert.IsNotNull(resources.GetUserById(resources.GetUserByUserName("TestUser").Id));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T05_GetUserByUuid()
        {
            try
            {
                Assert.IsNotNull(resources.GetUserByUuid(resources.GetUserByUserName("TestUser").UserUuid));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T06_GetUserByUserName()
        {
            try
            {
                Assert.IsNotNull(resources.GetUserByUserName("TestUser"));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T07_GetUserKeySetsByUser()
        {
            try
            {
                Assert.IsNotNull(resources.GetUserKeySetsByUser(resources.GetUserByUserName("TestUser")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T08_GetUserKeySetByEmail()
        {
            try
            {
                Assert.IsNotNull(resources.GetUserKeySetByEmail("testuser@centoso.com"));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T09_GetPublicKeysByUser()
        {
            try
            {
                Assert.IsNotNull(resources.GetPublicKeysByUser(resources.GetUserByUserName("TestUser")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T10_GetPublicKeyByEmail()
        {
            try
            {
                Assert.IsNotNull(resources.GetPublicKeyByEmail("pgp-public@centoso.com"));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T11_DeactivateUserKeySet()
        {
            try
            {
                Assert.IsTrue(resources.DeactivateUserKeySet(resources.GetUserKeySetByEmail("testuser@centoso.com")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T12_DeleteUser()
        {
            try
            {
                Assert.IsTrue(resources.DeleteUser(resources.GetUserByUserName("TestUser")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T13_DeleteUserKeySet()
        {
            try
            {
                Assert.IsTrue(resources.DeleteUserKeySet(resources.GetUserKeySetByEmail("testuser@centoso.com")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }

        [TestMethod]
        public void T14_DeletePublicKey()
        {
            try
            {
                Assert.IsTrue(resources.DeletePublicKey(resources.GetPublicKeyByEmail("pgp-public@centoso.com")));
            }
            catch (Exception ex)
            {
                Assert.IsNull(ex);
            }
        }
    }
}
